<?php

namespace Dont\Work\Too\Hard;

/**
 * A base class for common test functionality. You'll extend from this class instead of \Tlf\Tester (if you want)
 */
class Tester extends \Tlf\Tester {

    /**
     * Available at https://gitlab.com/taeluf/php/lildb 
     */
    protected \Tlf\LilDb $ldb;

    protected $BlogPosts = [
            [   'title'=>'Its gettin hot in here',
                'body'=>'I\'m talking about global warming. We need massive systemic change. Right to repair. An end to waste. Walkable cities. End of advertising? Idk... End to war. & so much more. Yes, you\'ll find my software littered with stuff like this.',
            ],
            [   'title'=>'Heckin cats',
                'body'=>'Cats are so cute & furry & I think they\'re livin almost the best life. If kitties could play video games ... that\'d be the dream, dog. <3 dogs too! Really really like bears. Baby grizzly bears.'
            ]
        ];


    protected function init(){
        $ldb = $this->ldb = \Tlf\Ldb::sqlite();
        $ldb->create('blog',['id'=>'int AUTO_INCREMENT', 'title'=>'VARCHAR(254)', 'body'=>'TEXT']);

        $ldb->insertAll('blog', $BlogPosts);
    }

}

